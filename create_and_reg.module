<?php
/**
 * @file
 * Main file of the module.
 */

/**
 * Implements hook_menu().
 */
function create_and_reg_menu() {
  $items['admin/config/workflow/create-and-reg'] = array(
    'title' => 'Create and register',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('create_and_reg_settings_form'),
    'access arguments' => array('administer create and reg'),
    'file' => 'create_and_reg.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function create_and_reg_permission() {
  return array(
    'administer create and reg' =>  array(
      'title' => t('Administer create and register'),
    ),
  );
}

/**
 * Implements hook_cron().
 */
function create_and_reg_cron() {
  // Send notification mail from the created nodes.
  $day_in_sec = 60 * 60 * 24;
  $notification_enable = variable_get('create_and_reg_email_notification_enable', 0);
  $last_notification_sent = variable_get('create_and_reg_last_notification_sent', 0);
  $notification_period = variable_get('create_and_reg_email_notification_period', $day_in_sec);
  if ($notification_enable && (REQUEST_TIME - $last_notification_sent >= $notification_period)) {
    create_and_reg_notificaion_email_send();
  }

  // Clear sessions from the DB.
  create_and_reg_clear_sessions();
}

/**
 * Implements hook_mail().
 */
function create_and_reg_mail($key, &$message, $params) {
  $pattern = '';
  // Build the patterns string.
  if (isset($params['nodes'])) {
    foreach ($params['nodes'] as $node) {
      $variables = array(
        '%node_url' => url('node/' . $node->nid, array('absolute' => TRUE)),
        '%node_type' => node_type_get_name($node),
        '%title' => $node->title,
      );
      $pattern .= strtr($params['created_nodes_pattern'], $variables);
    }
  }
  $body = strtr($params['message'], array('[created_nodes]' => $pattern));
  $message['subject'] = $params['subject'];
  $message['body'][] = drupal_html_to_text($body);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function create_and_reg_form_node_form_alter(&$form, &$form_state, $form_id) {
  global $user;

  // Get set types.
  $types = array();
  foreach (variable_get('create_and_reg_enabled_types', array()) as $type => $enable) {
    if ($enable) {
      $types[] = $type;
    }
  }

  // If the user not logged in, the node in the list and this will be a new node.
  if (!$user->uid && in_array($form['#node']->type, $types) && !isset($form['#node']->nid)) {
    // Store session id and add a custom submit handler.
    $form['create_and_reg_session_id'] = array(
      '#type' => 'value',
      '#value' => create_and_reg_get_session(),
    );
    $form['actions']['submit']['#submit'][] = 'create_and_reg_node_form_submit';

  }
}

/**
 * Writes the session into the database.
 */
function create_and_reg_node_form_submit($form, &$form_state) {
  $params = array(
    'nid' => $form_state['values']['nid'],
    'session_id' => $form_state['values']['create_and_reg_session_id'],
  );
  drupal_write_record('create_and_reg', $params);

  // Redirect to user login page.
  if (variable_get('create_and_reg_redirect', 1)) {
    $redirect_links = array(
      1 => 'user',
      2 => 'user/register',
      3 => variable_get('create_and_reg_redirect_other_url'),
    );
    // Token support for redirect url.
    $redirect_url = token_replace($redirect_links[variable_get('create_and_reg_redirect', 1)], array('node' => $form_state['node']));
    // Parse the link to support query and fragment attributes.
    $link = drupal_parse_url($redirect_url);
    $link['query']['create-and-reg'] = '1';
    $form_state['redirect'] = array(
      $link['path'],
      array(
        'query' => $link['query'],
        'fragment' => $link['fragment'],
      ),
    );
  }

  // Clear other status messages.
  if (variable_get('create_and_reg_disable_status_messages', 0)) {
    unset($_SESSION['messages']['status']);
  }

  // Show message to the user after he/she created the content.
  if ($after_message = t(variable_get('create_and_reg_after_message'))) {
    drupal_set_message(token_replace($after_message, array('node' => $form_state['node'])));
  }
}

/**
 * Implements hook_node_presave().
 *
 * Force publish the the node if it is set in the admin interface.
 */
function create_and_reg_node_presave($node) {
  // Get set types.
  $types = array();
  foreach (variable_get('create_and_reg_enabled_types', array()) as $type => $enable) {
    if ($enable) {
      $types[] = $type;
    }
  }
  if ($node->is_new && variable_get('create_and_reg_force_publish', 0) && in_array($node->type, $types) && $node->uid) {
    $node->status = NODE_PUBLISHED;
  }
}

/**
 * Implements hook_node_delete().
 */
function create_and_reg_node_delete($node) {
  db_delete('create_and_reg')
    ->condition('nid', $node->nid)
    ->execute();
}

/**
 * Implements hook_user_login().
 */
function create_and_reg_user_login(&$edit, $account) {
  $nids = create_and_reg_get_sessions_nodes();
  if (!empty($nids)) {
    create_and_reg_set_node_after_login($nids, $account->uid);
  }

  // Clear the user's cookie because we won't need it in the future.
  setcookie('create_and_reg', '', time() - 3600);
}

/**
 * Gets the node ids connected to the passed session.
 *
 * @param null $session_id
 *
 * @return
 *   Array of node ids.
 */
function create_and_reg_get_sessions_nodes($session_id = NULL) {
  $out = array();

  if (!$session_id) {
    $session_id = create_and_reg_get_session();
  }

  $query = db_select('create_and_reg', 'cr');
  $query->fields('cr', array('nid'));
  $query->join('node', 'n', 'cr.nid = n.nid');
  $query->condition('n.uid', 0);
  $query->condition('cr.session_id', $session_id);

  $result = $query->execute()->fetchAll(PDO::FETCH_ASSOC);

  // Order return value.
  foreach ($result as $res) {
    $out[] = $res['nid'];
  }

  return $out;
}

/**
 * Updates the nodes' data after the successfully login.
 *
 * Update the node's author and publish the node if it set in the admin
 * interface and mark the nodes as "assigned" in the database.
 *
 * @param $nids
 * @param $uid
 */
function create_and_reg_set_node_after_login($nids, $uid) {
  // Assign the nodes to the user.
  foreach (node_load_multiple($nids) as $node) {
    $node->uid = $uid;
    if (variable_get('create_and_reg_publish_after_reg')) {
      $node->status = 1;
    }
    node_save($node);

  }
  // Log that the node is assigned.
  db_update('create_and_reg')
    ->fields(array(
      'assigned' => 1,
    ))
    ->condition('nid', $nids, 'IN')
    ->execute();
}

/**
 * Clears stored sessions.
 */
function create_and_reg_clear_sessions() {
  $query = db_delete('create_and_reg')
    ->condition('assigned', 1);

  // Check email sent col if the email notification is enabled.
  if (variable_get('create_and_reg_email_notification_enable', 0)) {
    $query->condition('email_sent', 1);
  }

  $query->execute();
}

/**
 * Generates a session to the the user.
 *
 * @return string
 */
function create_and_reg_get_session() {
  $session = FALSE;

  if (!isset($_COOKIE['create_and_reg'])) {
    $session = md5(uniqid(rand(), TRUE));
    setcookie('create_and_reg', $session, 0, base_path());
  }

  return isset($_COOKIE['create_and_reg']) ? $_COOKIE['create_and_reg'] : $session;
}

/**
 * Returns with the not sent nodes' ids.
 */
function create_and_reg_get_unsent_nodes() {
  $query = db_select('create_and_reg', 'cr');
  $query->fields('cr', array('nid'));
  $query->condition('cr.email_sent', 0);

  return $query->execute()->fetchCol();
}

/**
 * Logs that the notification mail was sent from the nodes.
 *
 * @param $nids
 */
function create_and_reg_store_sent_nodes($nids) {
  db_update('create_and_reg')
    ->fields(array(
      'email_sent' => 1,
    ))
    ->condition('nid', $nids, 'IN')
    ->execute();
}

/**
 * Sends the notification mail from the created and unsent nodes.
 */
function create_and_reg_notificaion_email_send() {
  $nids = create_and_reg_get_unsent_nodes();

  if ($nids) {
    $to = variable_get('create_and_reg_email_notification_address', variable_get('site_mail', ini_get('sendmail_from')));
    $language = language_default();

    $params =  array(
      'nodes' => node_load_multiple($nids),
      'created_nodes_pattern' => variable_get('create_and_reg_email_notification_message_nodes', "%title\n%node_url\n\n"),
      'message' => variable_get('create_and_reg_email_notification_message', "The following nodes have been created since the last notification:\n[created_nodes]"),
      'subject' => variable_get('create_and_reg_email_notification_subject', 'New user generated contents on your site'),
    );

    $result = drupal_mail('create_and_reg', 'notification', $to, $language, $params);
    if ($result['result'] == TRUE) {
      create_and_reg_store_sent_nodes($nids);
      variable_set('create_and_reg_last_notification_sent', REQUEST_TIME);
    }
  }
}
