<?php
/**
 * @file
 * Create and registration module's admin section.
 */

/**
 * Generates the settings form.
 */
function create_and_reg_settings_form($form, &$form_state) {

  $form['create_and_reg_enabled_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled node types'),
    '#description' => t('Select the node types which you want to use with the module.'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('create_and_reg_enabled_types', array()),
  );

  $form['create_and_reg_redirect'] = array(
    '#type' => 'radios',
    '#title' => t('Redirect the user after the node is created'),
    '#default_value' => variable_get('create_and_reg_redirect', 1),
    '#options' => array(
      t("Don't redirect the user"),
      t('Redirect the user to the login page'),
      t('Redirect the user to the registration page'),
      t('Other url'),
    ),
  );
  $form['create_and_reg_redirect_other_url'] = array(
    '#type' => 'textfield',
    '#title' => t('The url where to redirect'),
    '#default_value' => variable_get('create_and_reg_redirect_other_url'),
    '#states' => array(
      'visible' => array(
       ':input[name="create_and_reg_redirect"]' => array('value' => '3'),
      ),
      'required' => array(
       ':input[name="create_and_reg_redirect"]' => array('value' => '3'),
      ),
    ),
  );

  $form['create_and_reg_disable_status_messages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable other status messages after the node is created'),
    '#default_value' => variable_get('create_and_reg_disable_status_messages', 0),
    '#description' => t('It will remove all status messages except the added below.')
  );

  $form['create_and_reg_publish_after_reg'] = array(
    '#type' => 'checkbox',
    '#title' => t('Publish the node after the successfully login'),
    '#default_value' => variable_get('create_and_reg_publish_after_reg', 0),
  );

  $form['create_and_reg_force_publish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force publish the content if creator is authenticated'),
    '#default_value' => variable_get('create_and_reg_force_publish', 0),
    '#description' => t("If you set the content to unpublished by default to avoid content without registration, the logged in user's content will also be unpublished. If you select this, the new content will be published if it has author."),
    '#states' => array(
      'visible' => array(
       ':input[name="create_and_reg_publish_after_reg"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['create_and_reg_after_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('This message will be displayed for the user after he/she created the content.'),
    '#default_value' => variable_get('create_and_reg_after_message'),
  );

  // Token module support.
  if (module_exists('token')) {
    $form['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['token_help']['help'] = array(
      '#theme' => 'token_tree',
      '#token_types' => array('node'),
    );
  }

  // Email notifications from the created nodes.
  $form['email_notification'] = array(
    '#title' => t('Email notifications'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['email_notification']['create_and_reg_email_notification_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable email notifications about the created nodes'),
    '#default_value' => variable_get('create_and_reg_email_notification_enable', 0),
  );
  $day_in_sec = 60 * 60 * 24;
  $form['email_notification']['create_and_reg_email_notification_period'] = array(
    '#type' => 'select',
    '#title' => t('Period'),
    '#options' => drupal_map_assoc(range($day_in_sec, 14 * $day_in_sec, $day_in_sec), 'format_interval'),
    '#default_value' => variable_get('create_and_reg_email_notification_period', $day_in_sec),
    '#states' => array(
      'visible' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
      'required' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
    ),
    '#description' => t('The notification will be sent in this period to the addresses.')
  );
  $form['email_notification']['create_and_reg_email_notification_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#default_value' => variable_get('create_and_reg_email_notification_address', variable_get('site_mail', ini_get('sendmail_from'))),
    '#states' => array(
      'visible' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
      'required' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['email_notification']['create_and_reg_email_notification_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('create_and_reg_email_notification_subject', 'New user generated contents on your site'),
    '#states' => array(
      'visible' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
      'required' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['email_notification']['create_and_reg_email_notification_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => variable_get('create_and_reg_email_notification_message', "The following nodes have been created since the last notification:\n\n[created_nodes]"),
    '#states' => array(
      'visible' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
      'required' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
    ),
    '#description' => t('The [created_nodes] pattern will be changed to the created nodes information.')
  );
  $form['email_notification']['create_and_reg_email_notification_message_nodes'] = array(
    '#type' => 'textarea',
    '#title' => t('Created nodes pattern'),
    '#default_value' => variable_get('create_and_reg_email_notification_message_nodes', "%title\n%node_url\n\n"),
    '#states' => array(
      'visible' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
      'required' => array(
       ':input[name="create_and_reg_email_notification_enable"]' => array('checked' => TRUE),
      ),
    ),
    '#description' => t('Build your [created_nodes] variable which will be used in the the message. You can use the following patterns: "%node_url", "%node_type", "%title"'),
  );

  return system_settings_form($form);
}

/**
 * Validates create_and_reg_settings_form.
 *
 * @param $form
 * @param $form_state
 */
function create_and_reg_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['create_and_reg_redirect'] == 3) {
    if (!trim($form_state['values']['create_and_reg_redirect_other_url'])) {
      form_set_error('create_and_reg_redirect_other_url', t('If you select other url redirect, the url field will be required.'));
    }
    elseif (!valid_url($form_state['values']['create_and_reg_redirect_other_url'])) {
      form_set_error('create_and_reg_redirect_other_url', t('The added url is not valid.'));
    }
    else {
      // Store the normal path instead of the url alias.
      $normal_path = drupal_get_normal_path($form_state['values']['create_and_reg_redirect_other_url']);
      if ($form_state['values']['create_and_reg_redirect_other_url'] != $normal_path) {
        drupal_set_message(t('%link_path has been stored as %normal_path', array('%link_path' => $form_state['values']['create_and_reg_redirect_other_url'], '%normal_path' => $normal_path)));
        $form_state['values']['create_and_reg_redirect_other_url'] = $normal_path;
      }
    }
  }

  // Notification email validation.
  if ($form_state['values']['create_and_reg_email_notification_enable']) {
    if (!valid_email_address($form_state['values']['create_and_reg_email_notification_address'])) {
      form_set_error('create_and_reg_email_notification_address', t('The added email address is not valid.'));
    }
  }
}
