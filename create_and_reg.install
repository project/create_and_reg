<?php
/**
 * @file
 * Database schema and update functions.
 */

/**
 * Implements hook_schema().
 */
function create_and_reg_schema() {
  $schema['create_and_reg'] = array(
    'description' => "Stores the node creators' session id.",
    'fields' => array(
      'nid' => array(
        'description' => 'The id of the created node.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0
      ),
      'session_id' => array(
        'description' => 'The session id of the creator.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'assigned' => array(
        'description' => 'A flag which shows that the node is assigned to a user or not.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ),
      'email_sent' => array(
        'description' => 'A flag which shows that the node is sent by email notification or not.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ),
    ),
    'indexes' => array(
      'session_id' => array('session_id'),
      'assigned' => array('assigned'),
      'email_sent' => array('email_sent'),
      'assigned_email_sent' => array('assigned', 'email_sent'),
    ),
    'foreign keys' => array(
      'node' => array(
        'table' => 'node',
        'columns' => array('nid' => 'nid'),
      ),
    ),
    'primary key' => array('nid'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function create_and_reg_uninstall() {
  variable_del('create_and_reg_enabled_types');
  variable_del('create_and_reg_redirect');
  variable_del('create_and_reg_after_message');
  variable_del('create_and_reg_disable_status_messages');
  variable_del('create_and_reg_redirect_other_url');
  variable_del('create_and_reg_publish_after_reg');
  variable_del('create_and_reg_force_publish');
  variable_del('create_and_reg_email_notification_enable');
  variable_del('create_and_reg_email_notification_period');
  variable_del('create_and_reg_email_notification_address');
  variable_del('create_and_reg_email_notification_subject');
  variable_del('create_and_reg_email_notification_message');
}

/**
 * Set the new redirect variable form the old one the delete.
 */
function create_and_reg_update_7001(&$sandbox) {
  variable_set('create_and_reg_redirect', variable_get('create_and_reg_redirect_to_login', 1));
  variable_del('create_and_reg_redirect_to_login');
}

/**
 * Add new fields and indexes to "create_and_reg" table.
 */
function create_and_reg_update_7002(&$sandbox) {
  $db_schema = drupal_get_schema('create_and_reg', TRUE);
  db_add_field('create_and_reg', 'assigned', $db_schema['fields']['assigned']);
  db_add_field('create_and_reg', 'email_sent', $db_schema['fields']['email_sent']);
  db_add_index('create_and_reg', 'assigned', $db_schema['indexes']['assigned']);
  db_add_index('create_and_reg', 'email_sent', $db_schema['indexes']['email_sent']);
  db_add_index('create_and_reg', 'assigned_email_sent', $db_schema['indexes']['assigned_email_sent']);
}
